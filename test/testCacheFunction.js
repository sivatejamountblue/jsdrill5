const cacheFunction= require("../cacheFunction");

function say(a,b,c){
   return (a+b+c);

}
let cache = cacheFunction(say);
console.log(cache(1,2,3))
console.log(cache(1,2,3))
console.log(cache(3,2,1))
console.log(cache(2,3,1))
console.log(cache(3,2,4))