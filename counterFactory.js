function counterFactory() {

    let i = 10;
    return {
        increment(){
            i++
            return i
        },
        decrement(){
            i--
            return i
        }
    }
    // Return an object that has two methods called `increment` and `decrement`.
    // `increment` should increment a counter variable in closure scope and return it.
    // `decrement` should decrement the counter variable and return it.
}

module.exports = counterFactory;