function limitFunctionCallCount(cb, n) {
    if ((!cb) || (!n)){
        return {}
    }
    let counter = 0;
    return function(a,b){
        if (counter < n){
            counter += 1;
            return cb(a,b);
        }
        else return null;
    }
    // Should return a function that invokes `cb`.
    // The returned function should only allow `cb` to be invoked `n` times.
    // Returning null is acceptable if cb can't be returned
}

module.exports = limitFunctionCallCount;